// set a timeout for 100 ms
let timeoutScheduled = Date.now();
setTimeout(() => {
  let delay = Date.now() - timeoutScheduled;
  console.log(`${delay} ms have passed since I was scheduled`);
}, 100);

// set another timeout of 95 ms
setTimeout(() => {
  let startCallback = Date.now()
  // do something that will take 10ms...
  while (Date.now() - startCallback < 10) {
    ; // do nothing
  }
}, 95)
