const fs = require('fs')

let readFileAsPromise = (filename => {
  return new Promise((resolve, reject) => {
    fs.readFile(filename, (err, data) => {
      if (err) {
        reject(err)
      } else {
        resolve(data)
      }
    })
  })
})

readFileAsPromise('./async.txt')
.then(data => {
  console.log(data.toString())
}).catch(err => {
  console.log(err)
})
