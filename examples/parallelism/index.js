let p1 = new Promise((resolve, reject) => {
  console.log('execute p1')
  setTimeout(resolve, 100, 'resolve p1');
});
let p2 = new Promise((resolve, reject) => {
  console.log('execute p2')
  setTimeout(resolve, 200, 'resolve p2');
});

Promise.all([p1, p2]).then(values => {
  console.log(values);
});
