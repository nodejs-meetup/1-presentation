const fs = require('fs')

function readMaxFiles(cb) {
  fs.readFile('/proc/sys/fs/file-max', cb)
}

readMaxFiles ((err, data) => {
  let maxFiles = parseInt(data.toString(), 10)
  console.log(`at max there may be ${maxFiles} file descriptors`)
  for (let i = 0; i < maxFiles; i++) {
    readMaxFiles((err, data) => {
      if (err) {
        throw err
      }
      // do nothing
    })
  }
});
