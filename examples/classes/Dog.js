class Dog {
  constructor (name) {
    this._name = name
  }
  get name () {
    return this._name
  }
  sayHello() {
    console.log('Bark')
  }
}

module.exports = Dog
