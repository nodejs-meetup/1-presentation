const fs = require('fs')
const bluebird = require('bluebird')

let readFileAsPromise = bluebird.promisify(fs.readFile)

readFileAsPromise('async.txt')
.then((data) => {
  console.log(data.toString())
}).catch((err) => {
  console.log(err)
})
