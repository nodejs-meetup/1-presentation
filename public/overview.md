# Node.js Düsseldorf Meetup


## Who we are and what we do


## What is this meetup about


### Introduction to Node.js
  * What is Node.js <!-- .element: class="fragment" data-fragment-index="1" -->
  * Why and when use Node.js <!-- .element: class="fragment" data-fragment-index="2" -->


### Node.js basics
  * Basics of programming language <!-- .element: class="fragment" data-fragment-index="1" -->
  * Package Management <!-- .element: class="fragment" data-fragment-index="2" -->
  * Node.js Event Loop <!-- .element: class="fragment" data-fragment-index="3" -->


### Node.js tutorial
  * Node.js project setup<!-- .element: class="fragment" data-fragment-index="1" -->
  * Unit testing with mocha and supertest <!-- .element: class="fragment" data-fragment-index="2" -->
  * Simple RESTful api with express.js<!-- .element: class="fragment" data-fragment-index="3" -->


### Sum up
  * Pitfalls<!-- .element: class="fragment" data-fragment-index="1" -->
  * Questions<!-- .element: class="fragment" data-fragment-index="2" -->
  * Extended Tutorial<!-- .element: class="fragment" data-fragment-index="3" -->
  * Next Steps<!-- .element: class="fragment" data-fragment-index="4" -->
