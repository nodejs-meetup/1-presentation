# Node.js basics


## Constants

    const a = [1, 2, 3, 4]
    a.push(5) // a = [1, 2, 3, 4, 5]
    a = 1 // TypeError: Assignment to constant variable.


## Block-scoped variables

    let a = 1 // a = 1
    {
      let a = 2 // a = 2
    }
    // a = 1


## Classes

    class Dog {
      constructor (name) {
        this._name = name
      }
      get name () {
        return this._name
      }
      sayHello() {
        console.log('Bark')
      }
    }
    let myDog = new Dog('Mia')
    console.log(myDog.name)
    myDog.sayHello()


## Arrow functions

    const a = 1
    let b = 1
    let f = c => a + b + c
    f(1) // 2


## Modules

*file add.js*

    module.exports = (a, b) => {
      return a + b
    }

*file index.js*

    const add = require('./add.js')
    add(1, 1) // 2


## Core modules

    const fs = require('fs')

    let readme = fs.readFileSync('README.md')


## 3rd party modules

[![3rd-party-modules](/img/ac6ob7u88wsl8qoxowf570ulu.png)](/player/?id=ac6ob7u88wsl8qoxowf570ulu)


## Manage dependencies with npm

    // create file package.json
    npm init

    // save dependency of module moment in package.json
    npm save moment --save

    // save dev-dependency of module in package.json
    npm save sinon --save-dev

    // 3rd party modules are installed in folder node_modules


## Manage dependencies with npm (demo)

[![manage-dependencies](/img/1vzocjjgthubyqg7c55eooaih.png)](/player/?id=1vzocjjgthubyqg7c55eooaih)


## Callbacks

    const fs = require('fs')

    fs.readFile('/etc/passwd', (err, data) => {
      if (err) {
        // handle error
      } else {
        // success, process data
      }
    })


## Callback-Hell

    makeSomethingAsync(function(err, a){
      if (err) {
        // handler error
      } else {
        makeSomethingMoreAsync(a, function(err, b){
          if (err) {
            // handle error
          } else {
            makeSomethingEvenMoreAsync(b, function(err, c){
              if (err) {
                // handle error
              } else {
                // Phew!
              }
            });
          }
        });
      }
    });


## Promises

      makeSomethingAsync
      .then(a => makeSomethingMoreAsync(a)
      ).then(b => makeSomethingEvenMoreAsync(b)
      ).then(c => {
        // ah, better
      }).catch(err => {
        // handle error  
      })  


### Turn function into promise producing function

    const fs = require('fs')

    let readFileAsPromise = (filename => {
      return new Promise((resolve, reject) => {
        fs.readFile(filename, (err, data) => {
          if (err) {
            reject(err)
          } else {
            resolve(data)
          }
        })
      })
    })


### Use new promise producing function

      readFileAsPromise('/etc/passwd')
      .then((data) => {
        // success, process data
      }).catch((err) => {
        // handle error
      })


### Promise producing function with bluebird

    const fs = require('fs')
    const bluebird = require('bluebird')

    let readFileAsPromise = bluebird.promisify(fs.readFile)


### A bunch of async functions

    let p1 = new Promise((resolve, reject) => {
      console.log('execute p1')
      setTimeout(resolve, 100, 'resolve p1');
    });
    let p2 = new Promise((resolve, reject) => {
      console.log('execute p2')
      setTimeout(resolve, 200, 'resolve p2');
    });

    Promise.all([p1, p2]).then(values => {
      console.log(values);
    });


### A bunch of async functions (demo)

[![parallelism](/img/f4qarmp4arnn4lo96jkcjd76y.png)](/player/?id=f4qarmp4arnn4lo96jkcjd76y)


## Event loop quiz

    setTimeout(function() {
      console.log("a");
    }, 10);

    setTimeout(function() {
      console.log("b");
    }, 0);

    setTimeout(function() {
      console.log("c");
    }, -1);

    setTimeout(function() {
      console.log("d");
    }, 0);

    console.log("e");

    //predict the output


## Event loop quiz (solution)

[![event-loop-quiz](/img/1e56wiile8lwminvn4n6jkcvg.png)](/player/?id=1e56wiile8lwminvn4n6jkcvg)


## The Node.js event loop

*"everything in Node.js is parallel except your code"*

  Felix Geisendörfer (core comitter of Node.js)
* only one line of JavaScript code will be executing at any time (single-threaded by using nonblocking techniques) <!-- .element: class="fragment" data-fragment-index="1" -->
* non-blocking operations can be archieved by offloading operations to the system kernel whenever possible. <!-- .element: class="fragment" data-fragment-index="2" -->


## Node.js event loop example

    1 const fs = require('fs')
    2 fs.readFile('async.txt', (err, data) => {
    3  console.log('file content', data.toString())
    4 })
    5 console.log('start reading file async.txt')

* process 1  <!-- .element: class="fragment" data-fragment-index="1" -->
* process 2 and put in background  <!-- .element: class="fragment" data-fragment-index="2" -->
* process 5 <!-- .element: class="fragment" data-fragment-index="3" -->
* not finished (2 was async), so wait for something to happen <!-- .element: class="fragment" data-fragment-index="4" -->
* execute callback in 3, when 2 has finished <!-- .element: class="fragment" data-fragment-index="5" -->


## Node.js event loop example (demo)

[![event-loop-example](/img/8wakbm1sl5scbvmk0763ywfb8.png)](/player/?id=8wakbm1sl5scbvmk0763ywfb8)


## after the initialization phase of Node.js programs, everything should be asynchron
