# Node.js tutorial


## 0 - init Node.js repository

    npm init


## 0 - install dependencies

    npm install express body-parser --save


##  0 - install dev dependencies

    npm install eslint mocha istanbul supertest --save-dev


## 0 - setup unit tests

*file package.json*

    ...
    "scripts": {
      ...
      "test": "istanbul cover _mocha",
      ...
    }
    ...


## 0 - setup linting

    node_modules/eslint/bin/eslint.js --init

*file package.json*

    ...
    "scripts": {
      ...
      "posttest": "eslint index.js lib"
      ...
    }
    ...


## 0 - setup configuration

*file config.js*

    module.exports = {
      port: 3000
    }


## 0 - initialization

*file index.js*

    const express = require('express')
    const restApi = require('./lib/restApi')
    const bodyParser = require('body-parser')
    const config = require('./config')

    let app = express()
    app.use(bodyParser.json())
    restApi(app)

    app.listen(config.port)
    console.log(`listening on port ${config.port}`)


## 1 - setup tests

*file test/restApiTest.js*

    const request = require('supertest')
    const express = require('express')
    const bodyParser = require('body-parser')
    const assert = require('assert')
    const restApi = require('../lib/restApi')
    let app

    beforeEach(() => {
      app = express()
      app.use(bodyParser.json())
      restApi(app)
    })


## 1 - add test for 404 handler

*file test/restApiTest.js*

    ...
    describe('app:', () => {
      it('should return 404 if no route matches', (done) => {
        request(app)
        .get('/')
        .end((err, res) => {
          assert.ifError(err)
          assert.equal(res.status, 404, 'status should be 404')
          assert.deepEqual(res.body, {error: 'Not Found'})
          done()
        })
      })
    })


## 2 - initialize module rest api

*file lib/restApi.js*

    module.exports = (app) => {}


## 2 - implementation of first test

*file lib/restApi.js*

    module.exports = (app) => {
      app.use((req, res, next) => {
        res.status(404)
        res.json({error: 'Not Found'})
      })
    }


## 3 - test error handler

*file test/restApiTest.js*

    ...
    it('should return 500 if error occurs', (done) => {
      let app = express()
      app.use((req, res, next) => {
        next(new Error('test'))
      })
      restApi(app)
      request(app)
      .get('/')
      .end((err, res) => {
        assert.ifError(err)
        assert.equal(res.status, 500, 'status should be 500')
        assert.deepEqual(res.body, {error: 'Internal Server Error'})
        done()
      })
    })


## 4 - implementation

*file lib/restApi.js*

    app.use((err, req, res, next) => {
      console.error(err)
      res.status(500)
      res.json({error: 'Internal Server Error'})
    })
    ...


## 5 - test for getting members

*file test/restApiTest.js*

    ...
    it('should get members', (done) => {
      request(app)
      .get('/members')
      .end((err, res) => {
        assert.deepEqual(res.body, [], 'should return members')
        done()
      })
    })
    ...


## 6 - implementation

*file lib/restApi.js*

    module.exports = (app) => {

      let members = []
      app.get('/members', (req, res, next) => {
        res.json(members)
      })
    ...


## 7 - test for creating a member

*file test/restApiTest.js*

    ...
    it('should create a member', (done) => {
      request(app)
      .post('/members')
      .send({test: 'test'})
      .end((err, res) => {
        assert.ifError(err)
        assert.deepEqual(res.body, {status: 'ok'}, 'should successfully insert member')
        request(app)
        .get('/members')
        .end((err, res) => {
          assert.ifError(err)
          assert.deepEqual(res.body, {test: 'test'}, 'should create a member')
        })
        done()
      })
    })
    ...


## 8 - implementation

*file lib/restApi.js*

    ...
    app.post('/members', (req, res, next) => {
      members.push(req.body)
      res.json(req.body)
    })
    ...
