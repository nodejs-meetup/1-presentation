# Pitfalls


### Event-Loop blocking

sometimes a timeout can take a little bit longer...

    // set a timeout for 100 ms
    let timeoutScheduled = Date.now();
    setTimeout(() => {
      let delay = Date.now() - timeoutScheduled;
      console.log(`${delay} ms have passed since scheduled`);
    }, 100);

    // set another timeout of 95 ms
    setTimeout(() => {
      let startCallback = Date.now()
      // do something that will take 10ms...
      while (Date.now() - startCallback < 10) {
        ; // do nothing
      }
    }, 95)


### Event-Loop blocking (demo)

[![event-loop-blocking](/img/aru9hden0gj4n1k861qof8x0w.png)](/player/?id=aru9hden0gj4n1k861qof8x0w)


### The Limited-Resource problem

Error: EMFILE: too many open files

    const fs = require('fs')

    function readMaxFiles(cb) {
      fs.readFile('/proc/sys/fs/file-max', cb)
    }

    readMaxFiles ((err, data) => {
      let maxFiles = parseInt(data.toString(), 10)
      console.log(`at max there may be ${maxFiles} 
        file descriptors`)
      for (let i = 0; i < maxFiles; i++) {
        readMaxFiles((err, data) => {
          if (err) {
            throw err
          }
          // do nothing
        })
      }
    });

Note:
* Use pooling


### The Limited-Resource problem (demo)

[![limited-resource-problem](/img/ephq6zjcn312609eo4tkh3jd5.png)](/player/?id=ephq6zjcn312609eo4tkh3jd5)


### Scope

Sometimes it is necessary to capture this:

    ...
    this.logger = logger;
    ...
    let self = this;
    MyEventEmitter.on('ready', function() {
      self.logger.info('ready for emitting events');
    });


### Licensing

view all licences your project depends on

    npm -g install licensecheck
    licensecheck


### Licensing (demo)

[![licensing](/img/7900515dn4smqeuq3vkgsgqpr.png)](/player/?id=7900515dn4smqeuq3vkgsgqpr)



# Many thanks for your attention!



# Questions?



# Extended tutorial


## Contents
* full rest api with MVC pattern <!-- .element: class="fragment" data-fragment-index="1" -->
* persistence layer with MongoDB <!-- .element: class="fragment" data-fragment-index="2" -->
* simple cli script for rest calls using the Node.js module commander <!-- .element: class="fragment" data-fragment-index="3" -->



# Next steps


## Security and Testing
  * validation
  * authentication
  * functional tests (using cucumber.js)


## CI / CD

* introduce CI / CD process for rest api from advanced tutorial
  * use docker
  * use GitLab CI
  * deploy to rancher


## Microservices

* microservices with Node.js
  * introduce Seneca


## ES6


## Webservice
* create frontend with Node.js webservice for Node.js Düsseldorf
