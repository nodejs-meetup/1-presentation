# Node.js introduction


## What is Node.js?

* Node.js is a server-side JavaScript runtime build on Chrome's V8 JavaScript engine. <!-- .element: class="fragment" data-fragment-index="1" -->
* Node.js is event-driven and non-blocking (Node.js event loop). <!-- .element: class="fragment" data-fragment-index="2" -->
* npm, the Node.js package ecosystem is a large ecosystem of open source
  libraries.  <!-- .element: class="fragment" data-fragment-index="3" -->

Note:
* This makes Node.js lightwight and efficient


## Why should I use Node.js?

* Node.js is easy to learn. <!-- .element: class="fragment" data-fragment-index="1" -->
* Node.js is fast to develop  <!-- .element: class="fragment" data-fragment-index="2" -->
* Node.js has a huge community <!-- .element: class="fragment" data-fragment-index="3" -->
* Node.js is fast <!-- .element: class="fragment" data-fragment-index="4" -->
* Node.js makes scalability easy <!-- .element: class="fragment" data-fragment-index="5" -->


## When should I use Node.js

* Node.js is especially good for I/O-bound programs. <!-- .element: class="fragment" data-fragment-index="1" -->
* Node.js is probably not the right choice for CPU-intensive operations  <!-- .element: class="fragment" data-fragment-index="2" -->
