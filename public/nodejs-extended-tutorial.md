# Node.js extended tutorial


## 0 - start mongodb server

    docker-compose up


## 0 - init Node.js repository

    npm init


## 0 - install dependencies

    npm install rc bunyan express mongoose request commander body-parser --save


## 0 - install dev dependencies

    npm install mocha istanbul eslint sinon supertest --save-dev


## 0 - setup unit tests

*file package.json*

    ...
    "scripts": {
      ...
      "test": "istanbul cover _mocha",
      ...
    }
    ...


## 0 - setup linting

    node_modules/eslint/bin/eslint.js --init

*file package.json*

    ...
    "scripts": {
      ...
      "posttest": "eslint index.js lib test"
      ...
    }
    ...


## 0 - configure linting

*file .eslintrc.js*

    module.exports = {
        "extends": "standard",
        "plugins": [
            "standard",
            "promise"
        ],
        "env": {
          "mocha": true
        }
    };


## 0 - setup configuration

*file config.js*

    module.exports = require('rc')('nodejs_tutorial', {
      logLevel: 'trace',
      port: 3000,

    })


## 1 - add cli script for testing

*file scripts/restcli.js*

    const request = require('request')
    const program = require('commander')
    const config = require('../config')

    program
      .option('-m, --method [method]', 'method (http verb)', 'GET')
      .option('-p, --path [path]', 'path of url to request', '/')
      .option('-d, --data [data]', 'data to send in body')
      .parse(process.argv)

    const requestOptions = {
      method: program.method,
      url: {
        protocol: 'http:',
        hostname: 'localhost',
        port: config.port,
        path: program.path
      }
    }

    if (program.data) {
      requestOptions.json = JSON.parse(program.data)
    }

    request(requestOptions, (err, res, body) => {
      if (err) {
        throw Error(err)
      } else {
        let response = body
        try {
          response = JSON.parse(body)
        } catch (e) {}
        console.log(res.statusCode)
        console.log(JSON.stringify(response, null, 2))
      }
    })


## 2 - create schema for mongodb

*file lib/models/Member.js*

    const mongoose = require('mongoose')
    const Schema = mongoose.Schema

    let MemberSchema = new Schema({
      name: String,
      company: String
    })
    let Member = mongoose.model('Member', MemberSchema)

    module.exports = Member


## 3 - initialization of app

*file index.js*

    const config = require('./config')
    const express = require('express')
    const bunyan = require('bunyan')
    const mongoose = require('mongoose')
    const Member = require('./lib/models/Member')
    const Members = require('./lib/controller/Members')
    const Router = require('./lib/Router')
    const App = require('./lib/App')

    mongoose.connect('mongodb://' + config.mongodbHost + '/members')

    let logger = bunyan.createLogger({
      name: 'nodejs-tutorial',
      level: config.logLevel
    })

    let members = new Members(Member, logger)
    let router = new Router(members, logger)
    let app = new App(config, express, router, logger)
    app.init()


## 4 - first unit test for class App

*file test/AppTest.js*

    const assert = require('assert')
    const sinon = require('sinon')
    const App = require('../lib/App')
    let configMock
    let expressInstanceMock
    let expressMock
    let routerMock
    let loggerMock
    let app

    describe('App:', () => {

      beforeEach(() => {
        configMock = {
          port: 3000
        }
        expressInstanceMock = {
          listen: sinon.spy()
        }
        expressMock = () => {
          return expressInstanceMock
        }
        routerMock = {
          addRoutes: sinon.spy()
        }
        loggerMock = {
          info: sinon.spy()
        }
        app = new App(configMock, expressMock, routerMock, loggerMock)
        app.init()
      })

      it('should exist', () => {
        assert(app)
      })
    })


## 5 - implementation of first test for class App

*file lib/App.js*

    class App {
      constructor (config, express, router, logger) {
        this.config = config
        this.express = express
        this.router = router
        this.logger = logger
      }

      init () {}
    }

    module.exports = App


## 6 - add unit test for listening on port

*file test/AppTest.js*

    ...
    it('should listen on the configured port after initialization', () => {
      assert(expressInstanceMock.listen.calledWith(configMock.port))
      assert(loggerMock.info.called)
    })
    ...


## 7 - implementation

*file lib/App.js*

    class App {
      constructor (config, express, router, logger) {
        this.config = config
        this.router = router
        this.Member = Member
        this.logger = logger
      }

      init () {
        this.app = this.express()
        this.app.listen(this.config.port)
        this.logger.info({port: this.config.port}, 'listening')
      }
    }

    module.exports = App


## 8 - add test for adding routes

*file test/AppTest.js*

    it('should add routes', () => {
      assert.deepEqual(routerMock.addRoutes.args[0][0], expressInstanceMock)
    })


## 9 - implementation

*file lib/App.js*

    class App {
      constructor (config, express, router, logger) {
        this.config = config
        this.express = express
        this.router = router
        this.logger = logger
      }

      init () {
        this.app = this.express()
        this.router.addRoutes(this.app)
        this.app.listen(this.config.port)
        this.logger.info({port: this.config.port}, 'listening')
      }
    }

    module.exports = App


## 10 - add first unit test for class Router

*file test/RouterTest.js*

    const assert = require('assert')
    const sinon = require('sinon')
    const express = require('express')
    const request = require('supertest')
    const Router = require('../lib/Router')

    let app
    let router
    let members
    let loggerMock

    describe('Router:', () => {
      beforeEach(() => {
        loggerMock = {
          error: sinon.spy()
        }
        memberMiddlewareMock = {}
        router = new Router(memberMiddlewareMock, loggerMock)
        app = express()
        router.addRoutes(app)
      })

      it('should exist', () => {
        assert(router)
      })
    })


## 11 - add implementation

*file lib/Router.js*

    class Router {
      constructor (members, logger) {
        this.members = members
        this.logger = logger
      }
      addRoutes (app) {}
    }

    module.exports = Router


## 12 - add test for 404 Not Found response

*file test/RouterTest.js*

    ...
    it('should respond with 404 Not Found JSON response if no route matches', (done) => {
      request(app)
      .get('/')
      .expect('Content-Type', /json/)
      .expect(404)
      .end((err, res) => {
        assert.ifError(err)
        assert.deepEqual(res.body, {error: 'Not Found'})
        done()
      })
    })
    ...


## 13 - implementation

*file lib/Router.js*

    class Router {
      addRoutes (app) {
        app.use((req, res, next) => {
          res.status(404)
          res.json({error: 'Not Found'})
        })
      }
    }

    module.exports = Router


## 14 - add test for 500 Internal Server Error response

*file test/RouterTest.js*

    it('should respond with 500 Internal Server Error JSON response if an error occured', (done) => {
      let router = new Router(members, loggerMock)
      let app = express()
      app.use((req, res, next) => {
        next(new Error('test'))
      })
      router.addRoutes(app)

      request(app)
      .get('/')
      .expect('Content-Type', /json/)
      .expect(500)
      .end((err, res) => {
        assert.ifError(err)
        assert(loggerMock.error.called)
        assert.deepEqual(res.body, {error: 'Internal Server Error'})
        done()
      })
    })


## 15 - implementation

    class Router {
      constructor (members, logger) {
        this.members = members
        this.logger = logger
      }
      addRoutes (app) {
        app.use((req, res, next) => {
          res.status(404)
          res.json({error: 'Not Found'})
        })
        app.use((err, req, res, next) => {
          res.status(500)
          res.json({error: 'Internal Server Error'})
          this.logger.error(err, 'router error handling')
        })
      }
    }

    module.exports = Router


## 16 - add test for getting members

*file test/RouterTest.js*

    const assert = require('assert')
    const sinon = require('sinon')
    const express = require('express')
    const request = require('supertest')
    const Router = require('../lib/Router')

    const MEMBERS = []
    const MEMBER = {}

    let app
    let router
    let members
    let loggerMock

    describe('Router:', () => {
      beforeEach(() => {
        loggerMock = {
          error: sinon.spy()
        }
        members = {
          getMembers: (req, res, next) => {
            res.json(MEMBERS)
          },
          createMember: (req, res, next) => {
            res.json(MEMBERS)
          },
          updateMember: (req, res, next) => {
            res.json(MEMBER)
          },
          deleteMember: (req, res, next) => {
            res.json(MEMBER)
          }
        }
        router = new Router(members, loggerMock)
        app = express()
        router.addRoutes(app)
      })

      it('should exist', () => {
        assert(router)
      })

      it('should respond with 404 Not Found JSON response if no route matches', (done) => {
        request(app)
        .get('/')
        .expect('Content-Type', /json/)
        .expect(404)
        .end((err, res) => {
          assert.ifError(err)
          assert.deepEqual(res.body, {error: 'Not Found'})
          done()
        })
      })

      it('should respond with 500 Internal Server Error JSON response if an error occured', (done) => {
        let router = new Router(members, loggerMock)
        let app = express()
        app.use((req, res, next) => {
          next(new Error('test'))
        })
        router.addRoutes(app)

        request(app)
        .get('/')
        .expect('Content-Type', /json/)
        .expect(500)
        .end((err, res) => {
          assert.ifError(err)
          assert(loggerMock.error.called)
          assert.deepEqual(res.body, {error: 'Internal Server Error'})
          done()
        })
      })

      it('should add route for getting Members', (done) => {
        request(app)
        .get('/members')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.ifError(err)
          assert.deepEqual(res.body, MEMBERS)
          done()
        })
      })
    })


## 17 - implementation

*file lib/Router.js*

    class Router {
      constructor (members, logger) {
        this.members = members
        this.logger = logger
      }
      addRoutes (app) {
        app.get('/members', this.members.getMembers.bind(this.members))
        app.use((req, res, next) => {
          res.status(404)
          res.json({error: 'Not Found'})
        })
        app.use((err, req, res, next) => {
          res.status(500)
          res.json({error: 'Internal Server Error'})
          this.logger.error(err, 'router error handling')
        })
      }
    }

    module.exports = Router


## 18 - add test for creating members

*file test/RouterTest.js*

    ...
    it('should add route for creating Members', (done) => {
      request(app)
      .post('/members')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        assert.ifError(err)
        assert.deepEqual(res.body, MEMBERS)
        done()
      })
    })


## 19 - implementation

*file lib/Router.js*

    const bodyParser = require('body-parser')

    class Router {
      constructor (members, logger) {
        this.members = members
        this.logger = logger
      }
      addRoutes (app) {
        app.use(bodyParser.json())
        app.get('/members', this.members.getMembers.bind(this.members))
        app.post('/members', this.members.createMember.bind(this.members))
        app.use((req, res, next) => {
          res.status(404)
          res.json({error: 'Not Found'})
        })
        app.use((err, req, res, next) => {
          res.status(500)
          res.json({error: 'Internal Server Error'})
          this.logger.error(err, 'router error handling')
        })
      }
    }

    module.exports = Router


## 20 - add test for update a member

*file test/RouterTest.js*

    ...
    it('should add route for updating a Member', (done) => {
      request(app)
      .put('/members/1')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        assert.ifError(err)
        assert.deepEqual(res.body, MEMBER)
        done()
      })
    })


## 21 - implementation

*file lib/Router.js*

    ...
    app.put('/members/:id', this.members.updateMember.bind(this.members))
    ...


## 22 - add test for deleting a member

*file test/RouterTest.js*

    ...
    it('should add route for deleting a Member', (done) => {
      request(app)
      .delete('/members/1')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        assert.ifError(err)
        assert.deepEqual(res.body, MEMBER)
        done()
      })
    })
    ...


## 23 - implementation

    app.delete('/members/:id', this.members.deleteMember.bind(this.members))


## 24 - add first test for class Members

*file test/MemberTest.js*

    const assert = require('assert')
    const sinon = require('sinon')
    const express = require('express')
    const request = require('supertest')
    const bodyParser = require('body-parser')

    const Members = require('../lib/controller/Members')

    const MEMBERS_DB_RESULT = [
      {
        _id: 1,
        name: 'test',
        company: 'test',
        __v: 0
      },
      {
        _id: 2,
        name: 'test',
        company: 'test',
        __v: 0
      }
    ]

    const MEMBERS = [
      {
        id: 1,
        name: 'test',
        company: 'test'
      },
      {
        id: 2,
        name: 'test',
        company: 'test'
      }
    ]

    const MEMBER_DB_RESULT = {
      _id: 1,
      name: 'test',
      company: 'test',
      __v: 0
    }

    const MEMBER = {
      id: 1,
      name: 'test',
      company: 'test'
    }

    let MemberModelMock
    let Members
    let app

    describe('Members:', () => {
      beforeEach(() => {
        MemberModelMock = function () {}
        MemberModelMock.find = sinon.stub().callsArgWith(1, undefined, MEMBERS_DB_RESULT)
        MemberModelMock.findOneAndUpdate = sinon.stub().callsArgWith(3, undefined, MEMBER_DB_RESULT)
        MemberModelMock.prototype.save = sinon.stub().callsArgWith(0, undefined, MEMBER_DB_RESULT)
        Members = new Members(MemberModelMock)
        app = express()
        app.use(bodyParser.json())
      })

      it('should exist', () => {
        assert(Members)
      })
    })


## 25 - implementation

*file lib/controller/members.js*

    class Members {
      constructor (MemberModel, logger) {
        this.MemberModel = MemberModel
        this.logger = logger
      }
    }

    module.exports = Members


## 26 - add tests for getting members

*file test/MemberTest.js*

    const assert = require('assert')
    const sinon = require('sinon')
    const express = require('express')
    const request = require('supertest')
    const bodyParser = require('body-parser')

    const MemberMiddleware = require('../lib/MemberMiddleware')

    const MEMBERS_DB_RESULT = [
      {
        _id: 1,
        name: 'test',
        company: 'test',
        __v: 0
      },
      {
        _id: 2,
        name: 'test',
        company: 'test',
        __v: 0
      }
    ]

    const MEMBERS = [
      {
        id: 1,
        name: 'test',
        company: 'test'
      },
      {
        id: 2,
        name: 'test',
        company: 'test'
      }
    ]

    const MEMBER_DB_RESULT = {
      _id: 1,
      name: 'test',
      company: 'test',
      __v: 0
    }

    const MEMBER = {
      id: 1,
      name: 'test',
      company: 'test'
    }

    let MemberModelMock
    let memberMiddleware
    let app

    describe('MemberMiddleware:', () => {
      beforeEach(() => {
        MemberModelMock = function () {}
        MemberModelMock.find = sinon.stub().callsArgWith(1, undefined, MEMBERS_DB_RESULT)
        MemberModelMock.prototype.save = sinon.stub().callsArgWith(0, undefined, MEMBER_DB_RESULT)
        memberMiddleware = new MemberMiddleware(MemberModelMock)
        app = express()
        app.use(bodyParser.json())
      })

      it('should exist', () => {
        assert(memberMiddleware)
      })

      it('should get members', (done) => {
        app.use(memberMiddleware.getMembers.bind(memberMiddleware))
        request(app)
        .get('/')
        .expect('Content-Type', /json/)
        .expect(200)
        .end((err, res) => {
          assert.ifError(err)
          assert.deepEqual(res.body, MEMBERS)
          done()
        })
      })
    })


## 27 - implementation

*file lib/controller/Members.js*

    ...
    getMembers (req, res, next) {
      this.MemberModel.find({}, (err, members) => {
        if (err) {
          next(err)
        } else {
          res.json(members.map((member) => {
            return {
              id: member._id,
              name: member.name,
              company: member.company
            }
          }))
        }
      })
    }
    ...


## 28 - add test for creating members

*file test/MemberTest.js*

    ...
    it('should create a member', (done) => {
      MemberModelMock.find =
      app.use(members.createMember.bind(members))
      request(app)
      .post('/')
      .send(MEMBER)
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        assert.ifError(err)
        assert.deepEqual(res.body, MEMBER)
        done()
      })
    })

    it('should forward error when save members causes error', (done) => {
      let mockError = new Error('test')
      MemberModelMock.prototype.save = sinon.stub().callsArgWith(0, mockError)
      app.use(members.createMember.bind(members))
      app.use((err, req, res, next) => {
        assert.deepEqual(err, mockError)
        res.json({})
      })
      request(app)
      .post('/')
      .send(MEMBER)
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        assert.ifError(err)
        done()
      })
    })
    ...


## 29 - implementation

*file lib/controller/Members.js*

    ...
    createMember (req, res, next) {
      let member = new this.MemberModel({
        name: req.body.name,
        company: req.body.company
      })
      member.save((err, member) => {
        if (err) {
          next(err)
        } else {
          res.json({
            id: member._id,
            name: member.name,
            company: member.company
          })
        }
      })
    }
    ...


## 30 - add test for updating a member

*file test/MemberTest.js*

    it('should update a member', (done) => {
      app.put('/:id', Members.updateMember.bind(Members))
      request(app)
      .put('/1')
      .send(MEMBER)
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        assert.ifError(err)
        assert.deepEqual(res.body, MEMBER)
        done()
      })
    })


## 31 - implementation

*file lib/controller/Members*

    updateMember (req, res, next) {
      this.MemberModel.findOneAndUpdate(
        {_id: req.params.id},
        req.body,
        {upsert: true},
        (err, member) => {
          if (err) {
            next(err)
          } else {
            res.json({
              id: member._id,
              name: member.name,
              company: member.company
            })
          }
        }
      )
    }


## 32 - add test for deleting a member

*file test/MemberTest*

    ...
    it('should delete a member', (done) => {
      MemberModelMock.find = sinon.stub().returns({
        remove: sinon.stub().callsArgWith(0, undefined, MEMBER_DB_RESULT)
      })
      app.delete('/:id', Members.deleteMember.bind(Members))
      request(app)
      .delete('/1')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        assert.ifError(err)
        assert.deepEqual(res.body, MEMBER)
        done()
      })
    })
    ...


## 33 - implementation

*file lib/contoller/Members.js*

    ...
    createMember (req, res, next) {
      let member = new this.MemberModel({
        name: req.body.name,
        company: req.body.company
      })
      member.save((err, member) => {
        if (err) {
          next(err)
        } else {
          res.json({
            id: member._id,
            name: member.name,
            company: member.company
          })
        }
      })
    }
    ...


## 34 - testing

    node index | bunyan


## 34 - testing

open a new terminal, than type

    node restcli.js -m GET -p /members  


## 34 - testing

    node restcli.js -m POST -p /members  -d '{"name":"christian","company":"its"}'
    node restcli.js -m POST -p /members  -d '{"name":"tobi","company":"its"}'

copy one id from response (referenced as $id)


## 34 - testing

    node restcli.js -m GET -p /members  

should now respond with 2 entries


## 34 - testing

    node restcli.js -m PUT -p /members/$id  -d '{"name":"chris"}'


## 34 - testing

    node restcli.js -m GET -p /members  

should now have 1 entry updated


## 34 - testing

    node restcli.js -m DELETE -p /members/$id  


## 34 - testing

    node restcli.js -m GET -p /members  

should have deleted 1 member
