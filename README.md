# Presentation for first Node.js meetup

The presentation contains all slides for the first Node.js meetup.
It uses the reveal.js Framework:

    https://github.com/hakimel/reveal.js.git
    Copyright (C) 2016 Hakim El Hattab, http://hakim.se, and reveal.js contributors

All slides in the tutorial section have numbers. They correspond to the
branch name in the tutorial: https://gitlab.com/nodejs-meetup/1-tutorial

# Installation

For installtion of Node.js dependencies run

    npm install

# Run

    npm start
