module.exports = function(grunt) {
	var port = grunt.option('port') || 8000;
	var root = grunt.option('root') || './public';

  grunt.initConfig({
    connect: {
      server: {
        options: {
          port: port,
          base: root,
          livereload: true,
          open: true
        }
      }
    },
    watch: {
      html: {
				files: root + '/*.html'
			},
			markdown: {
				files: root + '/*.md'
			},
      options: {
				livereload: true
			}
    }
  });
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.registerTask('default', ['connect', 'watch']);
  grunt.registerTask('serve', ['default']);
}
